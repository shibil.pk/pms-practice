
from django import forms

from tenants.models import DocumentType, Tenant, TenantDocument, TenantUnitAssign


class DocumentTypeForm(forms.ModelForm):

    class Meta:
        model = DocumentType
        exclude = ['creator', 'updater', 'date_updated', 'auto_id', 'is_deleted']

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}),
        }


class TenantForm(forms.ModelForm):

    class Meta:
        model = Tenant
        exclude = ['creator', 'updater', 'date_updated', 'auto_id', 'is_deleted']

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}),
            'address': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Address'}),
            'phone': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Phone'}),
            'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'}),
        }


class TenantDocumentForm(forms.ModelForm):

    class Meta:
        model = TenantDocument
        exclude = ['tenant']

        widgets = {
            'document_type': forms.Select(attrs={'class': 'required form-control', 'placeholder': 'TenantDocument Type'}),
        }


class TenantUnitAssignForm(forms.ModelForm):

    class Meta:
        model = TenantUnitAssign
        exclude = ['creator', 'updater', 'date_updated', 'auto_id', 'is_deleted']

        widgets = {
            'tenant': forms.Select(attrs={'class': 'required form-control', 'placeholder': 'TenantDocument Type'}),
            'unit': forms.Select(attrs={'class': 'required form-control', 'placeholder': 'TenantDocument Type'}),
            'agreement_end_date': forms.DateInput(attrs={'type': 'date', 'class': 'required form-control', 'placeholder': 'Agreement End Date'}),
            'upcoming_rent_date': forms.DateInput(attrs={'type': 'date', 'class': 'required form-control', 'placeholder': 'Agreement End Date'}),
        }
