import json
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.views.generic import TemplateView, View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponse
from core.functions import generate_form_errors
from core.decorators import role_required
from django.utils.decorators import method_decorator
from django.http import Http404
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.conf import settings as SETTINGS

from properties.models import Property, Unit
from tenants.models import TenantUnitAssign,Tenant
class CheckSuperRolesMixin(object):
    decorators = [login_required,role_required(['superuser'])]

    @method_decorator(decorators)
    def dispatch(self, request, *args, **kwargs):

        return super().dispatch(request, *args, **kwargs)


class App(CheckSuperRolesMixin, View):
    def get(self, request):

        return HttpResponseRedirect(reverse('dashboard'))


class DashboardView(CheckSuperRolesMixin, TemplateView):
    template_name = "base.html"

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        properties = Property.objects.all()
        units = Unit.objects.all()
        assigned_units = TenantUnitAssign.objects.filter(is_active=True)
        tenants = Tenant.objects.all()
        context = {
            "title": "Dashboard",
            "is_dashboard": True,
            "properties":properties,
            "units":units,
            "assigned_units":assigned_units,
            "tenants":tenants,
        }

        return context


def page_not_found(request, exception=None):

    context = {
        "title": "Page Not Found",
        'message': "The page you are looking for does not exist.",
        'error_code': 404

    }
    return render(request, 'errors/404.html', context)


def error(request, exception=None):

    context = {
        "title": "Error",
        'message': "Some error occurred.",
        'error_code': 500

    }
    return render(request, 'errors/500.html', context)


def permission_denied(request, exception=None):

    context = {

        "title": "Permission Denied",
        'message': "You have no permission to do this action.",
        'error_code': 403

    }
    return render(request, 'errors/403.html', context)


def bad_request(request, exception=None):

    context = {
        "title": "Bad Request",
        'message': "Bad request. Please try again",
        'error_code': 400

    }
    return render(request, 'errors/400.html', context)

def trigger_error(request):
    division_by_zero = 1 / 0

def error_log(request):
    SETTINGS.BASE_DIR
    path = f"{SETTINGS.BASE_DIR}/error.log"
    try:
        with open(path , 'r') as my_file:
            data=my_file.read()
        response = HttpResponse(content=data)
        response['Content-Type'] = 'text/plain'
    except:
        response = HttpResponse(content={'Error log not created yet.'})
        response['Content-Type'] = 'application/json'

    return response