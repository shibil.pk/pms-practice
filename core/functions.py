import string
import random
import math
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .services import generate_errors
from django.db.models import Max

def generate_unique_id(size=8, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def generate_order_id(auto_id=""):
    today = timezone.now()
    return str(today.year) + str(today.day).zfill(2) + str(today.month).zfill(2) + str(today.strftime("%I%M%s"))


generate_form_errors = generate_errors.form_errors
generate_serializer_errors = generate_errors.serializer_errors


def get_auto_id(model):
    return (model.all_objects.aggregate(max_auto_id=Max('auto_id')).get('max_auto_id') or 0) + 1

def random_string():
    return f"{string.ascii_letters}{string.digits}"


class CPaginator(Paginator):
    def validate_number(self, number):
        try:
            return super().validate_number(number)
        except EmptyPage:
            if int(number) > 1:
                return self.num_pages
            elif int(number) < 1:
                return 1
            else:
                return 1


def paginate(instances, request):
    try:
        page = int(request.GET.get('page', 1))
        items = int(request.GET.get('items', 50))
    except Exception as e:
        page = 1
        items = 50

    paginator = Paginator(instances, items)
    try:
        instances = paginator.page(page)
    except PageNotAnInteger:
        instances = paginator.page(1)
    except EmptyPage:
        instances = paginator.page(paginator.num_pages)

    data = {
        "count": instances.paginator.count,
        "num_pages": instances.paginator.num_pages
    }

    data['has_other_pages'] = instances.has_other_pages()
    if instances.has_other_pages():
        data['has_previous'] = instances.has_previous()
        if instances.has_previous():
            data['previous_page_number'] = instances.previous_page_number()

        data['has_next'] = instances.has_next()
        if instances.has_next():
            data['next_page_number'] = instances.next_page_number()
    return instances, data


def round_nearest(x, a):
    return round(round(x / a) * a, -int(math.floor(math.log10(a))))


def get_current_roles(user):

    user_roles = []
    if user.is_authenticated:

        if user.is_superuser:
            user_roles.append('superuser')

    return user_roles
