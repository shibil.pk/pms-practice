import ast
from calendar import c
import datetime
from django.template import Library
from django.contrib.auth.models import User
from django.template.defaultfilters import stringfilter
from django.db.models import Sum, Q, F, Max, Min, Case, F, When, CharField, Value
from django.utils import timezone


register = Library()


@register.filter
@stringfilter
def underscore_smallletter(value):
    value = value.replace(" ", "_")
    return value


@register.filter
def replace_underscore_upper(value):
    value = value.replace("_", " ").capitalize()
    return value


@register.filter
def to_fixed_two(value):
    return "{:10.2f}".format(value)


@register.filter
def split(value, seperator):
    if value:
        values = value.split(seperator)
        return list(filter(None, values))


@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    query = context['request'].GET.copy()
    for key in kwargs:
        query[key] = kwargs[key]
    return query.urlencode()


@register.filter
def iterator(ranges):
    if isinstance(ranges, str):
        _range = ranges.replace('range', '')
        _tuple = ast.literal_eval(_range)
        return range(_tuple[0], _tuple[1])
    else:
        return ranges