
from django import forms

from properties.models import Feature, Property, Unit


class FeatureForm(forms.ModelForm):

    class Meta:
        model = Feature
        exclude = ['creator', 'updater', 'date_updated', 'auto_id', 'is_deleted']

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}),
        }


class PropertyForm(forms.ModelForm):

    class Meta:
        model = Property
        exclude = ['creator', 'updater', 'date_updated', 'auto_id', 'is_deleted']

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}),
            'location': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Location'}),
            'address': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Address'}),
            'features': forms.SelectMultiple(attrs={'class': 'form-control', 'placeholder': 'Address'}),
        }


class UnitForm(forms.ModelForm):

    class Meta:
        model = Unit
        exclude = ['property']

        widgets = {
            'name': forms.TextInput(attrs={'class': 'required form-control', 'placeholder': 'Name'}),
            'unit_type': forms.Select(attrs={'class': 'required form-control', 'placeholder': 'Unit Type'}),
            'rent': forms.NumberInput(attrs={'class': 'required form-control', 'placeholder': 'Rent'}),
            'cost': forms.NumberInput(attrs={'class': 'required form-control', 'placeholder': 'Cost'})
        }
