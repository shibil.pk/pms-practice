from django.core.exceptions import ValidationError
from django.db import models

from core.models import BaseModel
from django.utils.translation import gettext_lazy as _
from django.core.validators import MinValueValidator
from decimal import Decimal
from django.core.validators import RegexValidator

PhoneRegexValidator = RegexValidator(r"^((\+91|91|0)[\- ]{0,1})?[456789]\d{9}$",
                                     message="Please Enter 10 digit mobile number or landline as 0<std code><phone number>", code="invalid_mobile")


class DocumentType(BaseModel):
    name = models.CharField(max_length=256)

    class Meta:
        verbose_name = _('Feature')
        verbose_name_plural = _('Features')

    def __str__(self):
        return self.name


class Tenant(BaseModel):
    name = models.CharField(max_length=256)
    address = models.TextField(null=True, blank=True)
    phone = models.CharField(max_length=14, null=True, blank=True, validators=[PhoneRegexValidator])
    email = models.EmailField(null=True, blank=True)

    class Meta:
        verbose_name = _('Tenant')
        verbose_name_plural = _('Tenant')
        ordering = ('name',)

    def __str__(self):
        return self.name


class TenantDocument(models.Model):

    document_type = models.ForeignKey(DocumentType, on_delete=models.CASCADE, limit_choices_to={'is_deleted': False})
    tenant = models.ForeignKey(Tenant, on_delete=models.CASCADE, limit_choices_to={'is_deleted': False})
    file = models.FileField(upload_to='documents/%Y/%m/%d/')

    def __str__(self):
        return self.tenant.name


class TenantUnitAssign(BaseModel):
    tenant = models.ForeignKey(Tenant, on_delete=models.CASCADE, limit_choices_to={'is_deleted': False})
    unit = models.ForeignKey('properties.Unit', on_delete=models.CASCADE)
    agreement_end_date = models.DateField()
    upcoming_rent_date = models.DateField()  # TODO: Add a logic to calculate next month's rent date
    agreement = models.FileField(upload_to='documents/%Y/%m/%d/', blank=True, null=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = _('TenantUnitAssign')
        verbose_name_plural = _('TenantUnitAssigns')

    def __str__(self):
        return self.tenant.name

    def clean(self):
        # Check if the tenant is already assigned to another unit
        if self.is_active and TenantUnitAssign.objects.filter(is_active=True, unit=self.unit).exclude(pk=self.pk).exists():
            raise ValidationError(_('This unit is already assigned to other tenant.'))
