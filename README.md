# Property Management System (PMS)

## Description
Brief description of the project.

## Setup
1. Clone the repository.
2. Install Python 3.11.2.
3. Create a virtual environment.
4. Install dependencies using `pip install -r requirements.txt`.
5. Create a `.env` file in the root directory with the following content:


6. Apply database migrations using `python manage.py migrate`.
7. Run the development server using `python manage.py runserver`.

## Environment Variables
- `SECRET_KEY`: Secret key for the Django project.
- `DEBUG`: Set to `on` for development, `off` for production.
- `ALLOWED_HOSTS`: Comma-separated list of allowed hosts.
- `SERVER`: Set to `on` to enable the server.
- `DATABASE_URL`: Database URL for the SQLite database.

## Usage
Instructions on how to use the project or any other relevant information.

## License
[Mohammed Shibil PK]
