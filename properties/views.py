import json

from django.db.models import Q
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView, View
from django.views.generic.detail import SingleObjectMixin
from django_filters.views import FilterView

from core.functions import CPaginator, generate_form_errors, get_auto_id
from core.views import CheckSuperRolesMixin
from properties.filters import FeatureFilter, PropertyFilter
from properties.forms import FeatureForm, PropertyForm, UnitForm
from properties.models import Feature, Property, Unit


class FeatureCreateView(CheckSuperRolesMixin, View):
    def get(self, *args, **kwargs):
        form = FeatureForm()

        context = {
            "form": form,
            "title": "Create Feature",
            "redirect": True,
            "active_menu": "feature",
        }
        return render(self.request, "properties/feature/feature_entry.html", context)

    def post(self, *args, **kwargs):

        form = FeatureForm(self.request.POST)

        if form.is_valid():

            data = form.save(commit=False)
            data.auto_id = get_auto_id(Feature)
            data.creator = self.request.user
            data.date_added = timezone.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully Created",
                "message": "Feature created successfully.",
                "redirect": "true",
                "redirect_url": reverse('properties:feature_list')
            }

        else:
            message = str(generate_form_errors(form, formset=False))

            response_data = {
                "status": "false",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


class FeatureUpdateView(CheckSuperRolesMixin, SingleObjectMixin, View):
    model = Feature

    def post(self, *args, **kwargs):
        self.object = self.get_object()
        form = FeatureForm(self.request.POST, instance=self.object)
        if form.is_valid():
            data = form.save(commit=False)
            data.updater = self.request.user
            data.date_updated = timezone.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully Updated",
                "message": "Feature Successfully Updated.",
                "redirect": "true",
                "redirect_url": reverse('properties:feature_list')
            }
        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        form = FeatureForm(instance=self.object)

        context = {
            "form": form,
            "title": "Edit Feature : " + self.object.__str__(),
            "instance": self.object,
            "redirect": True,
            "active_menu": "feature",
            "is_edit_page": True,
        }
        return render(self.request, "properties/feature/feature_entry.html", context)


class FeatureListView(CheckSuperRolesMixin, FilterView):
    model = Feature
    paginate_by = 21
    paginator_class = CPaginator
    template_name = "properties/feature/feature_list.html"
    context_object_name = "instances"
    filterset_class = FeatureFilter

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = "Feature"
        context["active_menu"] = "feature"

        return context


class FeatureDetailView(CheckSuperRolesMixin, DetailView):
    model = Feature
    template_name = "properties/feature/feature_view.html"
    context_object_name = "instance"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = "Feature"
        context["active_menu"] = "feature"
        context['instance_form_data'] = FeatureForm(instance=self.get_object())

        return context


class FeatureDeleteView(CheckSuperRolesMixin, SingleObjectMixin, View):
    model = Feature

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        self.object.is_deleted = True
        self.object.save()

        response_data = {
            "status": "true",
            "title": "Successfully Deleted",
            "message": "Feature Successfully Deleted.",
            "redirect": "true",
            "redirect_url": reverse('properties:feature_list')
        }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


class PropertyCreateView(CheckSuperRolesMixin, View):
    UnitFormset = formset_factory(UnitForm, extra=1)

    def get(self, *args, **kwargs):
        unit_formset = self.UnitFormset(prefix="unit_formset")
        form = PropertyForm()
        context = {
            "form": form,
            "title": "Create Property",
            "redirect": True,
            "active_menu": "property",
            'unit_formset': unit_formset
        }
        return render(self.request, "properties/property/property_entry.html", context)

    def post(self, *args, **kwargs):

        form = PropertyForm(self.request.POST)

        unit_formset = self.UnitFormset(self.request.POST, self.request.FILES, prefix='unit_formset', form_kwargs={'empty_permitted': False})

        if form.is_valid() and unit_formset.is_valid():

            data = form.save(commit=False)
            data.auto_id = get_auto_id(Property)
            data.creator = self.request.user
            data.save()
            form.save_m2m()

            for item in unit_formset:
                unit_data = item.save(commit=False)
                unit_data.property = data
                unit_data.save()

            response_data = {
                "status": "true",
                "title": "Successfully Created",
                "message": "Property created successfully.",
                "redirect": "true",
                "redirect_url": reverse('properties:property_list')
            }

        else:
            message = str(generate_form_errors(form, formset=False))
            message += str(generate_form_errors(unit_formset, formset=True))

            response_data = {
                "status": "false",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


class PropertyUpdateView(CheckSuperRolesMixin, SingleObjectMixin, View):
    model = Property

    def get_formset(self):
        instance = self.get_object()
        extra = 0 if Unit.objects.filter(property=instance).exists() else 1
        UnitFormset = inlineformset_factory(
            Property,
            Unit,
            can_delete=True,
            extra=extra,
            form=UnitForm,
        )
        return UnitFormset

    def post(self, *args, **kwargs):
        self.object = self.get_object()
        form = PropertyForm(self.request.POST, instance=self.object)
        UnitFormset = self.get_formset()
        unit_formset = UnitFormset(self.request.POST, self.request.FILES, instance=self.object, prefix='unit_formset', form_kwargs={'empty_permitted': False})
        if form.is_valid() and unit_formset.is_valid():
            data = form.save(commit=False)
            data.updater = self.request.user
            data.date_updated = timezone.now()
            data.save()
            form.save_m2m()

            for item in unit_formset:
                if item not in unit_formset.deleted_forms:
                    d_data = item.save(commit=False)
                    d_data.save()

            for f in unit_formset.deleted_forms:
                f.instance.delete()

            response_data = {
                "status": "true",
                "title": "Successfully Updated",
                "message": "Property Successfully Updated.",
                "redirect": "true",
                "redirect_url": reverse('properties:property_list')
            }
        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        form = PropertyForm(instance=self.object)
        UnitFormset = self.get_formset()
        unit_formset = UnitFormset(instance=self.object, prefix="unit_formset")
        context = {
            "form": form,
            "title": "Edit Property : " + self.object.__str__(),
            "instance": self.object,
            "redirect": True,
            "active_menu": "property",
            "is_edit_page": True,
            'unit_formset': unit_formset

        }
        return render(self.request, "properties/property/property_entry.html", context)


class PropertyListView(CheckSuperRolesMixin, FilterView):
    model = Property
    paginate_by = 21
    paginator_class = CPaginator
    template_name = "properties/property/property_list.html"
    context_object_name = "instances"
    filterset_class = PropertyFilter

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = "Property"
        context["active_menu"] = "property"

        return context


class PropertyDetailView(CheckSuperRolesMixin, DetailView):
    model = Property
    template_name = "properties/property/property_view.html"
    context_object_name = "instance"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = "Property"
        context["active_menu"] = "property"
        context['instance_form_data'] = PropertyForm(instance=self.get_object())

        return context


class PropertyDeleteView(CheckSuperRolesMixin, SingleObjectMixin, View):
    model = Property

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        self.object.is_deleted = True
        self.object.save()

        response_data = {
            "status": "true",
            "title": "Successfully Deleted",
            "message": "Property Successfully Deleted.",
            "redirect": "true",
            "redirect_url": reverse('properties:property_list')
        }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
