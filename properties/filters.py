import operator
from functools import reduce

import django_filters
from django.db.models import Q
from django.forms.widgets import Select, TextInput

from .models import Feature, Property, Unit


class FeatureFilter(django_filters.FilterSet):

    q = django_filters.CharFilter(label='Search',
                                  method='filter_by_search',
                                  widget=TextInput(attrs={'class': 'form-control', 'placeholder': 'Search'}))

    def filter_by_search(self, queryset, name, value):
        lookups = ['name__icontains', 'name__istartswith']
        or_queries = [Q(**{lookup: value}) for lookup in lookups]
        return queryset.filter(reduce(operator.or_, or_queries))

    class Meta:
        model = Feature
        fields = [
            'q',
        ]


class PropertyFilter(django_filters.FilterSet):

    q = django_filters.CharFilter(label='Search',
                                  method='filter_by_search',
                                  widget=TextInput(attrs={'class': 'form-control', 'placeholder': 'Search'}))

    unit_type = django_filters.ChoiceFilter(label='Unit Cost',
                                            method='filter_unit_type',
                                            choices=Unit.UNIT_CHOICES,
                                            widget=Select(attrs={'class': 'form-control', 'placeholder': 'Cost with in '}))

    def filter_by_search(self, queryset, name, value):
        lookups = [
            'name__icontains', 'name__istartswith',
            'unit__name__icontains', 'unit__name__istartswith',
            'features__name__icontains', 'features__name__istartswith',
        ]
        or_queries = [Q(**{lookup: value}) for lookup in lookups]
        queryset = queryset.filter(reduce(operator.or_, or_queries)).distinct()

        return queryset

    def filter_unit_type(self, queryset, name, value):
        property_ids = Unit.objects.filter(unit_type=value).values_list('property_id', flat=True).distinct()
        queryset = queryset.filter(pk__in=property_ids)
        return queryset

    class Meta:
        model = Property
        fields = [
            'q',
        ]
