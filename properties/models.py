from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from core.models import BaseModel
from tenants.models import TenantUnitAssign


class Feature(BaseModel):
    name = models.CharField(max_length=256)

    class Meta:
        verbose_name = _('Feature')
        verbose_name_plural = _('Features')

    def __str__(self):
        return self.name


class Property(BaseModel):
    name = models.CharField(max_length=256)
    address = models.TextField(null=True, blank=True)
    location = models.CharField(max_length=256)
    features = models.ManyToManyField(Feature, limit_choices_to={'is_deleted': False})

    class Meta:
        verbose_name = _('Property')
        verbose_name_plural = _('Properties')

    def __str__(self):
        return self.name

    def features_names(self):
        return ", ".join([i.name for i in self.features.all()])


class Unit(models.Model):
    UNIT_CHOICES = [
        ('1BHK', '1BHK'),
        ('2BHK', '2BHK'),
        ('3BHK', '3BHK'),
        ('4BHK', '4BHK'),
    ]

    name = models.CharField(max_length=255)
    unit_type = models.CharField("Unit Types", max_length=10, choices=UNIT_CHOICES, default='1BHK')  # TODO: Add dynamic choices
    property = models.ForeignKey(Property, on_delete=models.CASCADE, limit_choices_to={'is_deleted': False})
    rent = models.DecimalField(default=0, decimal_places=2,  max_digits=15, validators=[MinValueValidator(Decimal('0.00'))])
    cost = models.DecimalField(default=0, decimal_places=2,  max_digits=15, validators=[MinValueValidator(Decimal('0.00'))])

    def get_assigned_tenant(self):
        return TenantUnitAssign.objects.filter(is_active=True, unit=self).first()

    def __str__(self):
        return f"{self.name} - {self.get_unit_type_display()}"
