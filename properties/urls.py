
from django.urls import re_path
from properties import views

app_name = 'properties'

urlpatterns = [
    # Feature year URLs
    re_path(r'^features/create/$', views.FeatureCreateView.as_view(), name='feature_create'),
    re_path(r'^features/view/(?P<pk>[0-9a-f-]+)/$', views.FeatureDetailView.as_view(), name='feature_view'),
    re_path(r'^features/list/$', views.FeatureListView.as_view(), name='feature_list'),
    re_path(r'^features/update/(?P<pk>[0-9a-f-]+)/$', views.FeatureUpdateView.as_view(), name='feature_update'),
    re_path(r'^features/delete/(?P<pk>[0-9a-f-]+)/$', views.FeatureDeleteView.as_view(), name='feature_delete'),
    # Property year URLs
    re_path(r'^properties/create/$', views.PropertyCreateView.as_view(), name='property_create'),
    re_path(r'^properties/view/(?P<pk>[0-9a-f-]+)/$', views.PropertyDetailView.as_view(), name='property_view'),
    re_path(r'^properties/list/$', views.PropertyListView.as_view(), name='property_list'),
    re_path(r'^properties/update/(?P<pk>[0-9a-f-]+)/$', views.PropertyUpdateView.as_view(), name='property_update'),
    re_path(r'^properties/delete/(?P<pk>[0-9a-f-]+)/$', views.PropertyDeleteView.as_view(), name='property_delete'),
]
