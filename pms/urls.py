from django.contrib import admin
from django.urls import path
from django.contrib import admin
from django.conf import settings as SETTINGS
from django.conf.urls.static import static
from django.urls import include, re_path
from core import views as core_views



urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^app/accounts/', include('registration.backends.default.urls')),

    re_path(r'^$', core_views.App.as_view(), name='app'),
    re_path(r'^app/$', core_views.App.as_view(), name='app'),
    re_path(r'^app/dashboard/$', core_views.DashboardView.as_view(), name='dashboard'),

    re_path(r'^app/properties/', include('properties.urls')),
    re_path(r'^app/tenants/', include('tenants.urls')),





] + static(SETTINGS.STATIC_URL, document_root=SETTINGS.STATIC_ROOT) + static(SETTINGS.MEDIA_URL, document_root=SETTINGS.MEDIA_ROOT)

handler404 = 'core.views.page_not_found'
handler500 = 'core.views.error'
handler403 = 'core.views.permission_denied'
handler400 = 'core.views.bad_request'


