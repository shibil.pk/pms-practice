from django.contrib import admin
from .models import Mode


@admin.register(Mode)
class ModeAdmin(admin.ModelAdmin):
    model = Mode
    list_display = (
        'readonly',
        'maintenance',
        'down',
    )
