
from django.urls import re_path
from tenants import views

app_name = 'tenants'

urlpatterns = [
    # DocumentType year URLs
    re_path(r'^document-type/create/$', views.DocumentTypeCreateView.as_view(), name='document_type_create'),
    re_path(r'^document-type/view/(?P<pk>[0-9a-f-]+)/$', views.DocumentTypeDetailView.as_view(), name='document_type_view'),
    re_path(r'^document-type/list/$', views.DocumentTypeListView.as_view(), name='document_type_list'),
    re_path(r'^document-type/update/(?P<pk>[0-9a-f-]+)/$', views.DocumentTypeUpdateView.as_view(), name='document_type_update'),
    re_path(r'^document-type/delete/(?P<pk>[0-9a-f-]+)/$', views.DocumentTypeDeleteView.as_view(), name='document_type_delete'),
    # Tenant year URLs
    re_path(r'^tenant/create/$', views.TenantCreateView.as_view(), name='tenant_create'),
    re_path(r'^tenant/view/(?P<pk>[0-9a-f-]+)/$', views.TenantDetailView.as_view(), name='tenant_view'),
    re_path(r'^tenant/list/$', views.TenantListView.as_view(), name='tenant_list'),
    re_path(r'^tenant/update/(?P<pk>[0-9a-f-]+)/$', views.TenantUpdateView.as_view(), name='tenant_update'),
    re_path(r'^tenant/delete/(?P<pk>[0-9a-f-]+)/$', views.TenantDeleteView.as_view(), name='tenant_delete'),

    # Tenant year URLs
    re_path(r'^assign-tenant-unit/create/$', views.TenantUnitAssignCreateView.as_view(), name='tenant_unit_assign_create'),
    re_path(r'^assign-tenant-unit/list/$', views.TenantUnitAssignListView.as_view(), name='tenant_unit_assign_list'),
    re_path(r'^assign-tenant-unit/update/(?P<pk>[0-9a-f-]+)/$', views.TenantUnitAssignUpdateView.as_view(), name='tenant_unit_assign_update'),
    re_path(r'^assign-tenant-unit/delete/(?P<pk>[0-9a-f-]+)/$', views.TenantUnitAssignDeleteView.as_view(), name='tenant_unit_assign_delete'),
]
