import uuid
from django.db import models
from django.db.models import Max
from django.utils import timezone
from decimal import Decimal
from django.utils.translation import gettext_lazy as _

from django.core.validators import MinValueValidator
from django.contrib.auth import get_user_model

class ActiveManager(models.Manager):
    def get_queryset(self):
        return super(ActiveManager, self).get_queryset().exclude(is_deleted=True)


class BaseModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    auto_id = models.PositiveIntegerField(db_index=True, unique=True)
    creator = models.ForeignKey(get_user_model(), null=True, blank=True, related_name="creator_%(class)s_objects", limit_choices_to={'is_active': True}, on_delete=models.CASCADE)
    updater = models.ForeignKey(get_user_model(), null=True, blank=True, related_name="updater_%(class)s_objects", limit_choices_to={'is_active': True}, on_delete=models.CASCADE)
    date_added = models.DateTimeField(db_index=True, auto_now_add=True)
    date_updated = models.DateTimeField(null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    objects = ActiveManager()
    all_objects = models.Manager()
    class Meta:
        abstract = True

    def delete(self):
        self.is_deleted = True
        self.save()


class Mode(models.Model):
    readonly = models.BooleanField(default=False)
    maintenance = models.BooleanField(default=False)
    down = models.BooleanField(default=False)

    class Meta:
        db_table = 'mode'
        verbose_name = _('mode')
        verbose_name_plural = _('mode')
        ordering = ('id',)

    def __str__(self):
        return str(self.id)
