import json

from django.db.models import Q
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.views.generic import DetailView, View
from django.views.generic.detail import SingleObjectMixin
from django_filters.views import FilterView

from core.functions import CPaginator, generate_form_errors, get_auto_id
from core.views import CheckSuperRolesMixin
from tenants.filters import DocumentTypeFilter, TenantFilter, TenantUnitAssignFilter
from tenants.forms import (
    DocumentTypeForm,
    TenantDocumentForm,
    TenantForm,
    TenantUnitAssignForm,
)
from tenants.models import DocumentType, Tenant, TenantDocument, TenantUnitAssign


class DocumentTypeCreateView(CheckSuperRolesMixin, View):
    def get(self, *args, **kwargs):
        form = DocumentTypeForm()

        context = {
            "form": form,
            "title": "Create DocumentType",
            "redirect": True,
            "active_menu": "document_type",
        }
        return render(self.request, "tenants/document_type/document_type_entry.html", context)

    def post(self, *args, **kwargs):

        form = DocumentTypeForm(self.request.POST)

        if form.is_valid():

            data = form.save(commit=False)
            data.auto_id = get_auto_id(DocumentType)
            data.creator = self.request.user
            data.date_added = timezone.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully Created",
                "message": "DocumentType created successfully.",
                "redirect": "true",
                "redirect_url": reverse('tenants:document_type_list')
            }

        else:
            message = str(generate_form_errors(form, formset=False))

            response_data = {
                "status": "false",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


class DocumentTypeUpdateView(CheckSuperRolesMixin, SingleObjectMixin, View):
    model = DocumentType

    def post(self, *args, **kwargs):
        self.object = self.get_object()
        form = DocumentTypeForm(self.request.POST, instance=self.object)
        if form.is_valid():
            data = form.save(commit=False)
            data.updater = self.request.user
            data.date_updated = timezone.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully Updated",
                "message": "DocumentType Successfully Updated.",
                "redirect": "true",
                "redirect_url": reverse('tenants:document_type_list')
            }
        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        form = DocumentTypeForm(instance=self.object)

        context = {
            "form": form,
            "title": "Edit DocumentType : " + self.object.__str__(),
            "instance": self.object,
            "redirect": True,
            "active_menu": "document_type",
            "is_edit_page": True,
        }
        return render(self.request, "tenants/document_type/document_type_entry.html", context)


class DocumentTypeListView(CheckSuperRolesMixin, FilterView):
    model = DocumentType
    paginate_by = 21
    paginator_class = CPaginator
    template_name = "tenants/document_type/document_type_list.html"
    context_object_name = "instances"
    filterset_class = DocumentTypeFilter

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = "DocumentType"
        context["active_menu"] = "document_type"

        return context


class DocumentTypeDetailView(CheckSuperRolesMixin, DetailView):
    model = DocumentType
    template_name = "tenants/document_type/document_type_view.html"
    context_object_name = "instance"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = "DocumentType"
        context["active_menu"] = "document_type"
        context['instance_form_data'] = DocumentTypeForm(instance=self.get_object())

        return context


class DocumentTypeDeleteView(CheckSuperRolesMixin, SingleObjectMixin, View):
    model = DocumentType

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        self.object.is_deleted = True
        self.object.save()

        response_data = {
            "status": "true",
            "title": "Successfully Deleted",
            "message": "DocumentType Successfully Deleted.",
            "redirect": "true",
            "redirect_url": reverse('tenants:document_type_list')
        }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


class TenantCreateView(CheckSuperRolesMixin, View):
    TenantDocumentFormset = formset_factory(TenantDocumentForm, extra=1)

    def get(self, *args, **kwargs):
        tenant_document_formset = self.TenantDocumentFormset(prefix="tenant_document_formset")
        form = TenantForm()
        context = {
            "form": form,
            "title": "Create Tenant",
            "redirect": True,
            "active_menu": "tenant",
            'tenant_document_formset': tenant_document_formset
        }
        return render(self.request, "tenants/tenant/tenant_entry.html", context)

    def post(self, *args, **kwargs):

        form = TenantForm(self.request.POST)

        tenant_document_formset = self.TenantDocumentFormset(self.request.POST, self.request.FILES, prefix='tenant_document_formset', form_kwargs={'empty_permitted': False})

        if form.is_valid() and tenant_document_formset.is_valid():

            data = form.save(commit=False)
            data.auto_id = get_auto_id(Tenant)
            data.creator = self.request.user
            data.save()
            form.save_m2m()

            for item in tenant_document_formset:
                tenant_document_data = item.save(commit=False)
                tenant_document_data.tenant = data
                tenant_document_data.save()

            response_data = {
                "status": "true",
                "title": "Successfully Created",
                "message": "Tenant created successfully.",
                "redirect": "true",
                "redirect_url": reverse('tenants:tenant_list')
            }

        else:
            message = str(generate_form_errors(form, formset=False))
            message += str(generate_form_errors(tenant_document_formset, formset=True))

            response_data = {
                "status": "false",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


class TenantUpdateView(CheckSuperRolesMixin, SingleObjectMixin, View):
    model = Tenant

    def get_formset(self):
        instance = self.get_object()
        extra = 0 if TenantDocument.objects.filter(tenant=instance).exists() else 1
        TenantDocumentFormset = inlineformset_factory(
            Tenant,
            TenantDocument,
            can_delete=True,
            extra=extra,
            form=TenantDocumentForm,
        )
        return TenantDocumentFormset

    def post(self, *args, **kwargs):
        self.object = self.get_object()
        form = TenantForm(self.request.POST, instance=self.object)
        TenantDocumentFormset = self.get_formset()
        tenant_document_formset = TenantDocumentFormset(self.request.POST, self.request.FILES, instance=self.object, prefix='tenant_document_formset', form_kwargs={'empty_permitted': False})
        if form.is_valid() and tenant_document_formset.is_valid():
            data = form.save(commit=False)
            data.updater = self.request.user
            data.date_updated = timezone.now()
            data.save()
            form.save_m2m()

            for item in tenant_document_formset:
                if item not in tenant_document_formset.deleted_forms:
                    d_data = item.save(commit=False)
                    d_data.save()

            for f in tenant_document_formset.deleted_forms:
                f.instance.delete()

            response_data = {
                "status": "true",
                "title": "Successfully Updated",
                "message": "Tenant Successfully Updated.",
                "redirect": "true",
                "redirect_url": reverse('tenants:tenant_list')
            }
        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        form = TenantForm(instance=self.object)
        TenantDocumentFormset = self.get_formset()
        tenant_document_formset = TenantDocumentFormset(instance=self.object, prefix="tenant_document_formset")
        context = {
            "form": form,
            "title": "Edit Tenant : " + self.object.__str__(),
            "instance": self.object,
            "redirect": True,
            "active_menu": "tenant",
            "is_edit_page": True,
            'tenant_document_formset': tenant_document_formset

        }
        return render(self.request, "tenants/tenant/tenant_entry.html", context)


class TenantListView(CheckSuperRolesMixin, FilterView):
    model = Tenant
    paginate_by = 21
    paginator_class = CPaginator
    template_name = "tenants/tenant/tenant_list.html"
    context_object_name = "instances"
    filterset_class = TenantFilter

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = "Tenant"
        context["active_menu"] = "tenant"

        return context


class TenantDetailView(CheckSuperRolesMixin, DetailView):
    model = Tenant
    template_name = "tenants/tenant/tenant_view.html"
    context_object_name = "instance"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = "Tenant"
        context["active_menu"] = "tenant"
        context['instance_form_data'] = TenantForm(instance=self.get_object())

        return context


class TenantDeleteView(CheckSuperRolesMixin, SingleObjectMixin, View):
    model = Tenant

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        self.object.is_deleted = True
        self.object.save()

        response_data = {
            "status": "true",
            "title": "Successfully Deleted",
            "message": "Tenant Successfully Deleted.",
            "redirect": "true",
            "redirect_url": reverse('tenants:tenant_list')
        }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


class TenantUnitAssignCreateView(CheckSuperRolesMixin, View):
    def get(self, *args, **kwargs):
        form = TenantUnitAssignForm(initial=self.request.GET)

        context = {
            "form": form,
            "title": "Create TenantUnitAssign",
            "redirect": True,
            "active_menu": "tenant_unit_assign",
        }
        return render(self.request, "tenants/tenant_unit_assign/tenant_unit_assign_entry.html", context)

    def post(self, *args, **kwargs):

        form = TenantUnitAssignForm(self.request.POST)

        if form.is_valid():

            data = form.save(commit=False)
            data.auto_id = get_auto_id(TenantUnitAssign)
            data.creator = self.request.user
            data.date_added = timezone.now()
            data.save()
            redirect_url = self.request.GET.get('next', reverse('tenants:tenant_unit_assign_list'))
            response_data = {
                "status": "true",
                "title": "Successfully Created",
                "message": "TenantUnitAssign created successfully.",
                "redirect": "true",
                "redirect_url": redirect_url
            }

        else:
            message = str(generate_form_errors(form, formset=False))

            response_data = {
                "status": "false",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


class TenantUnitAssignUpdateView(CheckSuperRolesMixin, SingleObjectMixin, View):
    model = TenantUnitAssign

    def post(self, *args, **kwargs):
        self.object = self.get_object()
        form = TenantUnitAssignForm(self.request.POST, instance=self.object)
        if form.is_valid():
            data = form.save(commit=False)
            data.updater = self.request.user
            data.date_updated = timezone.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully Updated",
                "message": "TenantUnitAssign Successfully Updated.",
                "redirect": "true",
                "redirect_url": reverse('tenants:tenant_unit_assign_list')
            }
        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        form = TenantUnitAssignForm(instance=self.object)

        context = {
            "form": form,
            "title": "Edit TenantUnitAssign : " + self.object.__str__(),
            "instance": self.object,
            "redirect": True,
            "active_menu": "tenant_unit_assign",
            "is_edit_page": True,
        }
        return render(self.request, "tenants/tenant_unit_assign/tenant_unit_assign_entry.html", context)


class TenantUnitAssignListView(CheckSuperRolesMixin, FilterView):
    model = TenantUnitAssign
    paginate_by = 21
    paginator_class = CPaginator
    template_name = "tenants/tenant_unit_assign/tenant_unit_assign_list.html"
    context_object_name = "instances"
    filterset_class = TenantUnitAssignFilter

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["title"] = "TenantUnitAssign"
        context["active_menu"] = "tenant_unit_assign"

        return context


class TenantUnitAssignDeleteView(CheckSuperRolesMixin, SingleObjectMixin, View):
    model = TenantUnitAssign

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        self.object.is_deleted = True
        self.object.save()

        response_data = {
            "status": "true",
            "title": "Successfully Deleted",
            "message": "TenantUnitAssign Successfully Deleted.",
            "redirect": "true",
            "redirect_url": reverse('tenants:tenant_unit_assign_list')
        }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
