import operator
from functools import reduce

import django_filters
from django.db.models import Q
from django.forms.widgets import TextInput

from .models import DocumentType, Tenant, TenantUnitAssign


class DocumentTypeFilter(django_filters.FilterSet):

    q = django_filters.CharFilter(label='Search',
                                  method='filter_by_search',
                                  widget=TextInput(attrs={'class': 'form-control', 'placeholder': 'Search'}))

    def filter_by_search(self, queryset, name, value):
        lookups = ['name__icontains', 'name__istartswith']
        or_queries = [Q(**{lookup: value}) for lookup in lookups]
        return queryset.filter(reduce(operator.or_, or_queries))

    class Meta:
        model = DocumentType
        fields = [
            'q',
        ]


class TenantFilter(django_filters.FilterSet):

    q = django_filters.CharFilter(label='Search',
                                  method='filter_by_search',
                                  widget=TextInput(attrs={'class': 'form-control', 'placeholder': 'Search'}))

    def filter_by_search(self, queryset, name, value):
        lookups = [
            'name__icontains', 'name__istartswith',]
        or_queries = [Q(**{lookup: value}) for lookup in lookups]
        queryset = queryset.filter(reduce(operator.or_, or_queries))

        return queryset

    class Meta:
        model = Tenant
        fields = [
            'q',
        ]


class TenantUnitAssignFilter(django_filters.FilterSet):

    q = django_filters.CharFilter(label='Search',
                                  method='filter_by_search',
                                  widget=TextInput(attrs={'class': 'form-control', 'placeholder': 'Search'}))

    def filter_by_search(self, queryset, name, value):
        lookups = [
            'tenant_name__icontains', 'tenant_name__istartswith',
            'unit_name__icontains', 'unit_name__istartswith',
        ]
        or_queries = [Q(**{lookup: value}) for lookup in lookups]
        queryset = queryset.filter(reduce(operator.or_, or_queries))

        return queryset

    class Meta:
        model = TenantUnitAssign
        fields = [
            'q',
        ]
